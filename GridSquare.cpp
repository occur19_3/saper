#include "GridSquare.h"

#include <cassert>

GridSquare::GridSquare()
    : is_covered_(true)
    , has_mine_(false)
    , neighbouring_mines_(9u)
{}

void GridSquare::Uncover()
{
    assert(is_covered_);
    is_covered_ = false;
}

void GridSquare::PlaceMine()
{
    assert(!has_mine_);
    assert(neighbouring_mines_ == 9u); //we either a field to have mine or nieghbouring mines
    has_mine_ = true;
}

void GridSquare::SetNeighbouringMines(unsigned neighbouring_mines)
{
    assert(!has_mine_);
    assert(neighbouring_mines_ == 9u); // safety check for not placing this field twice
    assert(neighbouring_mines <= 8u);
    neighbouring_mines_ = neighbouring_mines;
}
