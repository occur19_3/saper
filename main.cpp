#include <iostream>

#include "Grid.h"
#include "GridSquare.h"

std::string print_square(const GridSquare& square, bool cover)
{
    if (cover && square.IsCovered()) {
        return " ";
    }

    if (square.HasMine()) {
        return "x";
    }

    return std::to_string(square.GetNeighbouringMines());
}

void print_grid(const Grid &grid, bool cover, unsigned x, unsigned y) {
    for (unsigned j = 0; j <= 2 * y; ++j) {
        for (unsigned i = 0; i <= 2 * x; ++i) {
            if (j % 2 == 0) {
                std::cout << "-";
            } else if (i % 2 == 0) {
                std::cout << "|";
            } else  {
                std::cout << print_square(grid.GetSquare(i / 2, j / 2), cover);
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    Grid g(5, 7);
    g.PlaceMines(0.10);
    g.SetMineCount();
    print_grid(g, false, 5, 7);
    print_grid(g, true, 5, 7);
    g.OnClick(2, 2);
    print_grid(g, false, 5, 7);
    print_grid(g, true, 5, 7);

    g.OnClick(2, 4);
    print_grid(g, false, 5, 7);
    print_grid(g, true, 5, 7);
    return 0;
//    for (size_t i = 0; i < 5; ++i) {
//        for (size_t j = 0; j < 5; ++j) {
//            g.OnClick(i, j);
//            print_grid(g, 5, 5);
//        }
//    }
//    return 0;
}
