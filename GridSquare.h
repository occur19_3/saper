#pragma once

class GridSquare
{
public:
    GridSquare();

    void Uncover();
    void PlaceMine();
    void SetNeighbouringMines(unsigned neighbouring_mines);

    bool IsCovered() const { return is_covered_; }
    bool HasMine() const { return has_mine_; }
    unsigned GetNeighbouringMines() const { return neighbouring_mines_; }

private:
    bool is_covered_;
    bool has_mine_;
    unsigned neighbouring_mines_;
};
