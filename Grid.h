#pragma once

#include <functional>

class GridSquare;

/* In this code I validate the arguments moslty through assertions,
 * in production code I would consider returning error values if I knew that input could be erroneus
 * or throwing exceptions(though in my opinion error messages are more clear) */
class Grid {
public:
    Grid(unsigned x, unsigned y);
    ~Grid();

    /// we want to limit the ability of modifying fields through squares hence const access
    /// squares are indexed from 0
    const GridSquare & GetSquare(unsigned x, unsigned y) const;

    void PlaceMines(float percentage = 0.20);

    void SetMineCount();

    // returns if the field contained a mine
    bool OnClick(unsigned x, unsigned y);

private:
    unsigned GetSize() const;

    GridSquare & GetSquare(unsigned x, unsigned y);

    unsigned GetIndex(unsigned x, unsigned y) const;

    void ApplyOnNeighbours(unsigned x, unsigned y, std::function<void(unsigned, unsigned)> f);

    const unsigned x_, y_;
    GridSquare *grid_;
};
