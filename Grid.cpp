#include "Grid.h"

#include "GridSquare.h"

#include <algorithm>
#include <chrono>
#include <cassert>
#include <deque>
#include <random>
#include <set>


Grid::Grid(unsigned x, unsigned y)
    : x_(x)
    , y_(y)
{
    grid_ = new GridSquare[GetSize()];
}

Grid::~Grid()
{
    delete[] grid_;
}

const GridSquare & Grid::GetSquare(unsigned x, unsigned y) const
{
    return grid_[GetIndex(x, y)];
}


void Grid::PlaceMines(float percentage) {
    assert(percentage >= 0.0f);
    assert(percentage <= 1.0f);
    unsigned mine_no = static_cast<unsigned>(GetSize() * percentage);

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::minstd_rand0 gen(seed);

    std::vector<unsigned> values(GetSize());
    std::iota(values.begin(), values.end(), 0);
    std::shuffle(values.begin(), values.end(), gen);
    for (size_t i = 0; i < mine_no; ++i) {
        grid_[values[i]].PlaceMine();
    }
}

void Grid::SetMineCount()
{
    for (unsigned j = 0; j < y_; ++j) {
        for (unsigned i = 0; i < x_; ++i) {
            GridSquare & sq = GetSquare(i, j);
            if (sq.HasMine()) {
                continue;
            }
            unsigned count = 0;
            ApplyOnNeighbours(i, j, [this, &count] (unsigned x, unsigned y) {
                if (GetSquare(x, y).HasMine())
                    ++count;
            });
            sq.SetNeighbouringMines(count);
        }
    }
}

bool Grid::OnClick(unsigned x, unsigned y) {
    assert(x < x_);
    assert(y < y_);
    GridSquare & sq = GetSquare(x, y);
    if (sq.HasMine()) {
        for (unsigned i = 0; i < GetSize(); ++i) {
            if (grid_[i].HasMine()) {
                grid_[i].Uncover();
            }
        }
        return true;
    }

    std::deque<std::pair<unsigned, unsigned>> neighbours{{x,y}};
    while (!neighbours.empty()) {
        std::pair<unsigned, unsigned> s = neighbours.front();
        neighbours.pop_front();
        GridSquare &sq = GetSquare(s.first, s.second);
        if (!sq.IsCovered() || sq.HasMine()) {
            continue;
        }
        sq.Uncover();
        if (sq.GetNeighbouringMines() == 0) {
            ApplyOnNeighbours(s.first, s.second, [&neighbours] (unsigned x_n, unsigned y_n) {
                neighbours.emplace_back(x_n, y_n);
            });
        }
    }
    return false;
}

void Grid::ApplyOnNeighbours(unsigned x, unsigned y, std::function<void(unsigned, unsigned)> f)
{
    unsigned start_x = x == 0        ? x : x - 1;
    unsigned end_x =   x == (x_ - 1) ? x : x + 1;

    unsigned start_y = y == 0        ? y : y - 1;
    unsigned end_y =   y == (y_ - 1) ? y : y + 1;
    for (unsigned j = start_y; j <= end_y; ++j) {
        for (unsigned i = start_x; i <= end_x; ++i) {
            if (i == x && j == y) {
                continue;
            }
            f(i, j);
        }
    }
}

unsigned Grid::GetSize() const
{
    return x_ * y_;
}

GridSquare & Grid::GetSquare(unsigned x, unsigned y)
{
    assert(x < x_);
    assert(y < y_);
    return grid_[GetIndex(x, y)];
}

unsigned Grid::GetIndex(unsigned x, unsigned y) const
{
    return x + y * (x_);
}
